<?php


namespace Tareas\Controllers;
use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;

use Tareas\Models\Categorias;
use Tareas\Models\Tareas;
use Tareas\Models\TareasCategorias;


class ControllerBase extends Controller
{
	public function initialize()
	{
		date_default_timezone_set('Europe/Madrid');
		setlocale(LC_ALL, 'es_ES.UTF-8');
 		$this->view->setTemplateAfter('default');

		$headerCollection = $this->assets->collection('header');
		$headerCollection->addCss('css/bootstrap.min.css');
		$headerCollection->addCss('css/paneles.css');

		$footerCollection = $this->assets->collection('footer');

		$footerCollection->addJs('js/jquery.min.js');
		$footerCollection->addJs('js/tareas.js');


	}

	public function thrown404() {
   	 return $this->dispatcher->forward([
   		 'controller' => 'errores',
   		 'action' => 'notFound',
   	 ]);
    }
}
