<?php

namespace Tareas\Controllers;
use Phalcon\Http\Request;
use Phalcon\Http\Response;
use Tareas\Models\Tareas as Tareas;
use Tareas\Models\TareasCategorias;
use Tareas\Models\Categorias;


class IndexController extends ControllerBase
{

    public function indexAction()
    {
        if ($this->request->get('q')!=null) {
            $this->view->disable();
            $q = filter_var($this->request->get('q'),FILTER_SANITIZE_MAGIC_QUOTES);
            $sql= "select * from tareas where nombre like '%".$q."%'";
            $tareas = $this->db->query($sql)->fetchAll();
            $tareas = $this->_categorias($tareas);
            return json_encode(['mensaje'=>'ok','texto'=>$tareas]);
        } else {

            $this->view->tareas = $this->_categorias(Tareas::find()->toArray());
        }

    }

    public function insertarAction() {
        $this->view->disable();
        if ($this->request->isPost()) {
            $categorias = $this->_cogerCategorias();

            $tarea = new Tareas();
            $tarea->nombre= filter_var($this->request->getPost('nombre'),FILTER_SANITIZE_MAGIC_QUOTES);
            $tarea->estado=0;
            if ($tarea->save()) {
                if ($this->_guardarCategorias($tarea->id,$categorias)){
                    $arrTarea = [0=>['id'=>$tarea->id]];
                    $tareas= $this->_categorias($arrTarea);
                    $cadena='<tr id="'.$tarea->id.'"><td>'. $tarea->nombre .'</td><td>'.$tareas[0]['categorias'].'</td><td><button class="btn btn-lg btn-primary btn-block borrar" type="submit">borrar</button></td></tr><tr id="ultimo"></tr>';
                    $retrono =
                     [
                        "mensaje"=>'ok',
                        'texto'=>$cadena
                    ];
                    return json_encode($retrono);
                } else {
                    return json_encode(['mensaje'=>'error','texto'=>'No se han insertado las categortias']);
                }
            } else {
                return json_encode(['mensaje'=>'error','texto'=>'No se ha insertado la Tarea']);
            }
        }

    }
    private function _cogerCategorias() {
        return explode(',',$this->request->getPost('categorias'));
    }
    private function _guardarCategorias($id,$categorias) {
        foreach($categorias as $categoria) {
            $tareaCategoria = new TareasCategorias();
            $tareaCategoria->id_tarea = $id;
            $tareaCategoria->id_categoria = $categoria;
            $tareaCategoria->save();
        }
        return true;
    }

    public function borrarAction($id) {
         $this->view->disable();
        $id = filter_var($id,FILTER_SANITIZE_MAGIC_QUOTES);
        $sql = "delete from tareas_categorias where id_tarea=".$id;
        if ($this->db->execute($sql)) {
            $sql = "delete from tareas where id = ".$id;

            if (!$this->db->execute($sql)) {
                $messages = $tarea->getMessages();
                $texto="";
                foreach ($messages as $message) {
                    $texto .= $message. "\n";
                }
                return json_encode(['mensaje'=>'error','texto'=>$texto]);
            }
            return json_encode(['mensaje'=>'ok','texto'=>"Borrado Corectamente"]);
        } else {
            return json_encode(['mensaje'=>'error',$texto='Error al borrar categortias']);
        }
    }
    private function _categorias ($tareas) {
        foreach ($tareas as $k => $tarea) {
            $sql = "select * from categorias inner join tareas_categorias on tareas_categorias.id_categoria = categorias.id
                where id_tarea =".$tarea['id'];
             $categorias = $this->db->query($sql)->fetchAll();

             $tmp_categorias = "";
             foreach($categorias as $categoria) {
                 if ($tmp_categorias !='') {
                     $tmp_categorias .=',';
                 }
                 $tmp_categorias .=$categoria['nombre'];
             }
             $tareas[$k]['categorias'] = $tmp_categorias;
        }
        return $tareas;
    }
}
