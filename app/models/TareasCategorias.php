<?php
namespace Tareas\Models;

class TareasCategorias extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id_tarea;

    /**
     *
     * @var integer
     */
    public $id_categoria;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tareas");
        $this->setSource("tareas_categorias");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tareas_categorias';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return TareasCategorias[]|TareasCategorias|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return TareasCategorias|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
