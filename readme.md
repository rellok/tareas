El proyecto a sido desarrollado con php7.2, phalcon3.4, mysql 8 , jquery y Bootstrap.

para deplegar el proyecto:
No he creado un desplegador de proyectos. ni docker ni capistrano , ni phing y tampoco ningun script.

Requisitos del proyecto:
php = 72
phalcon = 3.4
mysql >5.7
apache > 2.4 o nginx >1.16
composer

instalacion:
1.-  git clone git@bitbucket.org:rellok/tareas.git
2.- cd tareas
3.- composer install
4.- ejecutar script de bd ---> mysql -uroot -p < script.sql
5.- añadir sitio a apache o nginx:
	yo trabajo con apache y php-fpm:

	server {
	     listen       80;
	     listen       [::]:80;
	     server_name  tareas.loc www.tareas.loc;

	     root         /home/relok/www/tareas/public;
	     index index.php index.html;


		rewrite_log on;

		access_log   /var/log/nginx/tareas/access.log main;
	    error_log   /var/log/nginx/tareas/error.log;


		try_files $uri $uri/ @rewrite;

		location @rewrite {
		    rewrite ^/(.*)$ /index.php?_url=$uri&$args;
		}



		location ~ \.php$ {

			try_files $uri =404;
			fastcgi_intercept_errors on;
			fastcgi_split_path_info ^(.+\.php)(/.+)$;
			fastcgi_pass unix:/var/run/php72.sock;
			fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
		        fastcgi_param   APPLICATION_ENV  development;
			include /etc/nginx/fastcgi_params;
		}


		location ~ /(\.ht|\.user.ini|\.git|\.hg|\.bzr|\.svn) {
			deny all;
		}
	}
6.- Añadir a /etc/hosts:
	127.0.0.0 www.tareas.loc tareas.loc

7.- crear directorios necesarios:
	mkdir tmp
	mkdir tmp/logs
	mkdir tmp/cache
