$(document).ready(function() {
	$('#insertar').on('click', function(event){
		event.preventDefault();
		var categorias="";
		if ($('#categoria1').is(':checked')) {
			categorias="1";
		}
		if ($('#categoria2').is(':checked')) {
			if (categorias!='') {
				categorias=categorias+','
			}
			categorias = categorias +'2';
		}
		if ($('#categoria3').is(':checked')) {
			if (categorias!='') {
				categorias=categorias+','
			}
			categorias = categorias +'3';
		}

		$.ajax({
           type: "POST",
           url: "/index/insertar",
		   data:{'nombre':$("#inputNombre").val(),'categorias':categorias},
           success: function(retorno)
           {

			   var valor = JSON.parse (retorno);
			   if (valor.mensaje=='ok') {

			   		$('#ultimo').prop('outerHTML',valor.texto);
					$('#inputNombre').val('');
					$('#categoria1').prop('check',false);
					$('#categoria2').prop('check',false);
					$('#categoria3').prop('check',false);

			   } else {
				   $('#error').html(valor.texto);
			   }

           }
         });
		 return false;
	})
	$('.borrar').on('click', function(event) {
		event.preventDefault();
		var id=$(this).parent().parent().attr('id');
		var url= "/index/borrar/"+id;
		$.ajax({
           type: "GET",
           url: url,

           success: function(retorno)
           {
			  var valor = JSON.parse (retorno);
			   if (valor.mensaje=='ok') {
				   $('#'+id).remove();
			   }
			   $('#error').html(valor.texto);
           }
		});
	});
	$('#bbuscar').on('click', function(event) {
		event.preventDefault();
		$.ajax({
           type: "GET",
           url: '/index?q='+$('#q').val(),
           success: function(retorno)
           {
			  var valor = JSON.parse (retorno);
			  let txt ="<tr><td>Tarea</td><td>Categorias</td><td>Borrar</td></tr>";
			  valor.texto.forEach(function (valor) {
		  		 txt =txt+'<tr id="'+valor.id+'"><td>'+valor.nombre+'</td><td>'+valor.categorias+'</td><td><button class="btn btn-lg btn-primary btn-block borrar" type="submit">borrar</button></td></tr>';
			 });
			  txt +='<tr id="ultimo"></tr>';
			  $('#buscar').html(txt);
			  $('.borrar').on('click', function(event) {
		  		event.preventDefault();
		  		var id=$(this).parent().parent().attr('id');
		  		var url= "/index/borrar/"+id;
		  		$.ajax({
		             type: "GET",
		             url: url,

		             success: function(retorno)
		             {
		  			  var valor = JSON.parse (retorno);
		  			   if (valor.mensaje=='ok') {
		  				   $('#'+id).remove();
		  			   }
		  			   $('#error').html(valor.texto);
		             }
		  		});
		  	});

		   }
	  });
  	});

});
